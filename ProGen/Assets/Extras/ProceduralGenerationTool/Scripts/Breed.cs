﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breed : MonoBehaviour {
    public GameObject Babe;
    public Transform SpawnSpot1;
    public Transform SpawnSpot2;
    public Transform SpawnSpot3;
    [SerializeField] int _childCount = 2;

    private void OnTriggerEnter (Collider other) {
        if (other.gameObject.tag == "DADDY" && _childCount == 1) {
            Instantiate (Babe, SpawnSpot1.position, Quaternion.identity);
        }
        if (other.gameObject.tag == "DADDY" && _childCount == 2) {
            Instantiate (Babe, SpawnSpot1.position, Quaternion.identity);
            Instantiate (Babe, SpawnSpot2.position, Quaternion.identity);
        }

        if (other.gameObject.tag == "DADDY" && _childCount == 3) {
            Instantiate (Babe, SpawnSpot1.position, Quaternion.identity);
            Instantiate (Babe, SpawnSpot2.position, Quaternion.identity);
            Instantiate (Babe, SpawnSpot3.position, Quaternion.identity);
        }
    }
}