﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MIxieMo : MonoBehaviour {
    public Material _babyMat;
    public Color _momColor;
    public float _momNoise;
    public GameObject _mom;
    public Color _dadColor;
    public float _dadNoise;
    public GameObject _dad;
    public MeshRenderer _rend;


    private void Start () {
        _babyMat = _rend.material;
        _momColor = _mom.GetComponent<fuckityRe> ().randomC;
        _dadColor = _dad.GetComponent<fuckityRe> ().randomC;
        _momNoise = _mom.GetComponent<fuckityRe> ().NoiseSize;
        _dadNoise = _dad.GetComponent<fuckityRe> ().NoiseSize;
    }
    private void Update () {
        
        _babyMat.SetColor ("Mommy", _momColor);
        _babyMat.SetColor ("Daddy", _momColor);
        _babyMat.SetFloat ("MommySpots", _momNoise);
        _babyMat.SetFloat ("DaddySpots", _dadNoise);
    }

}