﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnObjectF : MonoBehaviour
{
    public Button _Spawn;
    public GameObject availableObject;
    public Transform SpawnSpot;
    private int _valueF = 20;
    void Start () {
        _Spawn.onClick.AddListener (SpawnThis);
    }

    void Update () {
        gameObject.GetComponent<Wallet> ();
    }

    public void SpawnThis () {
        if (Wallet._instance._startingBalance >= 0 && Wallet._instance._startingBalance >= _valueF) {
            Instantiate (availableObject, SpawnSpot.position, Quaternion.identity);
        }
    }
}
