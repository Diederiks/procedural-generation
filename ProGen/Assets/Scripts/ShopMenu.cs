﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopMenu : MonoBehaviour {
    public GameObject _relevantCanvas;

    private void Start() {
        _relevantCanvas.SetActive(false);
    }

    public void SetThisActive () {
        _relevantCanvas.SetActive (true);
    }
    public void PickMe () {
        _relevantCanvas.SetActive (false);
    }
}