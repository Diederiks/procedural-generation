﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class ParticleManager : MonoBehaviour
{
    public Component _particleSystems;
    public Button _ActivateParticle;
    public Button _otherOption1;
    public Button _otherOption2;
    void Start()
    {
        _particleSystems.GetComponent<ParticleSystem>().enableEmission = false;
        _ActivateParticle.onClick.AddListener(ChangeWeather);
        _otherOption1.onClick.AddListener(TurnItOff);
        _otherOption2.onClick.AddListener(TurnItOff);
    }

    public void ChangeWeather()
    {
         _particleSystems.GetComponent<ParticleSystem>().enableEmission = true;
         gameObject.GetComponent<AudioSource> ().Play ();
    }

    public void TurnItOff(){
        _particleSystems.GetComponent<ParticleSystem>().enableEmission = false;
        gameObject.GetComponent<AudioSource> ().Stop ();
    }
}
