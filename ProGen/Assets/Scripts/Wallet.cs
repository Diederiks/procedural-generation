﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wallet : MonoBehaviour {
    public static Wallet _instance;
    public Text _moneyBalance;
    public int _startingBalance = 100;
    public int _fishWorth = 50;
    //public Button _selling1;
    public Button _selling2;
    public Button _selling3;
    public int _assetA = 200;
    public int _assetB = 250;
    public int _assetC = 150;
    public int _assetD = 100;
    public int _assetE = 75;
    public int _assetF = 20;
    public GameObject _needMoneyCanvas;

    private void Awake () {
        DontDestroyOnLoad (_moneyBalance);
    }

    void Start () {
        _instance = this;
        //_selling1.onClick.AddListener (SoldFish);
        _selling2.onClick.AddListener (SoldFish);
        _selling3.onClick.AddListener (SoldFish);
        _needMoneyCanvas.SetActive (false);
    }

    void Update () {
        _startingBalance = Mathf.Clamp (_startingBalance, 0, 999999999);
        if (_startingBalance <= 0) {
            _needMoneyCanvas.SetActive (true);
        }
        _moneyBalance.text = _startingBalance.ToString ();
        if (_startingBalance > 0) {
            _needMoneyCanvas.SetActive (false);
        }
    }

    public void SoldFish () {
        _startingBalance += _fishWorth;
    }

    public void BuyingA () {
        if (_startingBalance > 0 && _startingBalance >= _assetA) {
            _startingBalance -= _assetA;
        }
    }
    public void BuyingB () {
        if (_startingBalance > 0 && _startingBalance >= _assetB) {
            _startingBalance -= _assetB;
        }

    }
    public void BuyingC () {
        if (_startingBalance > 0 && _startingBalance >= _assetC) {
            _startingBalance -= _assetC;
        }
    }
    public void BuyingD () {
        if (_startingBalance > 0 && _startingBalance >= _assetD) {
            _startingBalance -= _assetD;
        }
    }
    public void BuyingE () {
        if (_startingBalance > 0 && _startingBalance >= _assetE) {
            _startingBalance -= _assetE;
        }
    }
    public void BuyingF () {
        if (_startingBalance > 0 && _startingBalance >= _assetF) {
            _startingBalance -= _assetF;
        }
    }

}