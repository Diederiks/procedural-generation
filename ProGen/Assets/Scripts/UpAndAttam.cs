﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UpAndAttam : MonoBehaviour {
    private float _zPos;
    private Vector3 offSet;
    private Camera MainCamera;
    private bool dragging;
    [SerializeField] public UnityEvent onBeginDrag;
    [SerializeField] public UnityEvent onEndDrag;
    public Component _poofParticles;
    public bool selected = false;
    public GameObject _itemManagementCanvas;

    public Button closeItemMC;

    private void Awake () {
        _poofParticles.transform.position = gameObject.transform.position;
    }

    void Start () {
        _itemManagementCanvas.SetActive (false);
        closeItemMC.onClick.AddListener(ClosetheItemManager);
        MainCamera = Camera.main;
        _poofParticles.transform.position = gameObject.transform.position;
        gameObject.GetComponent<AudioSource> ().Play ();
        _zPos = MainCamera.WorldToScreenPoint (transform.position).z;
    }
    void Update () {
        if (dragging) {
            Debug.Log ("Is dragging");
            Vector3 position = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, _zPos);
            transform.position = MainCamera.ScreenToWorldPoint (position + new Vector3 (offSet.x, offSet.y));
        }
    }

    void OnMouseDown () {
        selected = true;
        if (!dragging) {
            BeginDrag ();
        }
        if (selected == true) {
            _itemManagementCanvas.SetActive (true);
        }
    }
    void OnMouseUp () {
        EndDrag ();
    }

    public void BeginDrag () {
        onBeginDrag.Invoke ();
        dragging = true;
        offSet = MainCamera.WorldToScreenPoint (transform.position) - Input.mousePosition;
    }

    public void EndDrag () {
        onEndDrag.Invoke ();
        dragging = false;
    }
    void OnCollisionEnter (Collision collision) {
        if (collision.gameObject.tag == "Trash") {
            Destroy (this.gameObject);
        }
    }

    public void ClosetheItemManager(){
        _itemManagementCanvas.SetActive(false);
    }
}