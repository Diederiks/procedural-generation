﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuOfMenus : MonoBehaviour
{
    public GameObject _shopMenu;
    public GameObject _bank;
    public GameObject _miniMenu;
    public Button _onButton;
    public Button _offButton;
    private bool _on;


    void Start()
    {
        _on = true;
        _onButton.onClick.AddListener(On);
        _offButton.onClick.AddListener(Off);
        _shopMenu.SetActive(true);
        _bank.SetActive(true);
        _miniMenu.SetActive(true);
    }

   
    public void On()
    {
        _on = true;
        _shopMenu.SetActive(true);
        _bank.SetActive(true);
        _miniMenu.SetActive(true);
    }

    public void Off(){
        _on = false;
        _shopMenu.SetActive(false);
        _bank.SetActive(false);
        _miniMenu.SetActive(false);
    }
}
