﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomSpawn : MonoBehaviour {
    public Button _Spawn;
    public GameObject availableObject;
    public Transform SpawnSpot;

    void Start () {
        _Spawn.onClick.AddListener (SpawnThis);
    }

    public void SpawnThis () {

        Instantiate (availableObject, SpawnSpot.position, Quaternion.identity);

    }
}