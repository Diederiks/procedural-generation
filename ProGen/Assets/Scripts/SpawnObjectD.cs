﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnObjectD : MonoBehaviour
{
    public Button _Spawn;
    public GameObject availableObject;
    public Transform SpawnSpot;
    public int _valueD = 100;
    void Start () {
        _Spawn.onClick.AddListener (SpawnThis);
    }

    void Update () {
        gameObject.GetComponent<Wallet> ();
    }

    public void SpawnThis () {
        if (Wallet._instance._startingBalance >= 0 && Wallet._instance._startingBalance >= _valueD) {
            Instantiate (availableObject, SpawnSpot.position, Quaternion.identity);
        }
    }
}
