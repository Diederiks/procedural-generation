﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnObjectE : MonoBehaviour
{
    public Button _Spawn;
    public GameObject availableObject;
    public Transform SpawnSpot;
    void Start () {
        _Spawn.onClick.AddListener (SpawnThis);
    }

    void Update () {
        gameObject.GetComponent<Wallet> ();
    }

    public void SpawnThis () {
        if (Wallet._instance._startingBalance >= 0 && Wallet._instance._startingBalance >= Wallet._instance._assetE) {
            Instantiate (availableObject, SpawnSpot.position, Quaternion.identity);
        }
    }
}
