﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mix : MonoBehaviour {
    public GameObject _MommyA;
    public GameObject _DaddyB;
    Renderer _randy;
    public Material a;
    public Material b;
    Material basemat;
    [SerializeField] float duration = 2.0f;

    // Start is called before the first frame update
    void Start () {
        _randy = GetComponent<Renderer> ();
        _randy.material = basemat;
        a = _MommyA.GetComponent<Renderer> ().material;
        b = _DaddyB.GetComponent<Renderer> ().material;
    }

    // Update is called once per frame
    void Update () {
        float lerp = Mathf.PingPong (Time.time, duration) / duration;
        _randy.material.Lerp(a,b,lerp);
    }
}