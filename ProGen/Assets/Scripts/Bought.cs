﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bought : MonoBehaviour
{
    public Text _costOfAsset;
    public int _costValue;
    private int _currentAccountbalance;
    public GameObject _moneyManager;
    
    void Start()
    {
        _costValue = int.Parse(_costOfAsset.text);
        _currentAccountbalance = _moneyManager.GetComponent<Wallet>()._startingBalance;
    }


    public void Buying(){
        _currentAccountbalance -= _costValue;
    }
}
