﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MatMix : MonoBehaviour {
    public Image _MommyA;
    public Image _DaddyB;
    Color lerpedColor = Color.white;
    public Color a;
    public Color b;
    public Color _babymat;
    public Image _baby;

    void Update () {
        a = _MommyA.GetComponent<Image> ().color;
        b = _DaddyB.GetComponent<Image> ().color;
        _babymat = Color.Lerp (a, b, 0.5f);
    }
    public void Mixing () {
        _baby.GetComponent<Image> ().color = _babymat;
    }
}