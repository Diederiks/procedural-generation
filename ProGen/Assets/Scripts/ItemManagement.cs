﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemManagement : MonoBehaviour
{
    public Button Rotatio;
    public Button Deletion;

    private void Start() {
        Rotatio.onClick.AddListener(RotateObject);
        Deletion.onClick.AddListener(DeleteObject);
    }
    public void RotateObject()
    {
        gameObject.transform.rotation *= Quaternion.Euler(0,+45,0);
    }

    public void DeleteObject()
    {
        Destroy(gameObject);
    }
}
