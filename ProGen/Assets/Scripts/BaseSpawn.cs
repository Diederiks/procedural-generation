﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseSpawn : MonoBehaviour
{
    Renderer _Rend;
    public Button _spawn;

    void Start () {
        _Rend = GetComponent<Renderer> ();
        _spawn.onClick.AddListener(GenerateC);
        GenerateC();
    }

    public void GenerateC () {
        _Rend.material.SetColor ("_Color", Random.ColorHSV ());
    }
}
