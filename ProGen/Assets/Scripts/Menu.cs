﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour {
    public GameObject BreedingCanvas;

    private void Start () {
        BreedingCanvas.SetActive (false);
    }
    public void BreedingRoom () {
        BreedingCanvas.SetActive (true);
    }

    public void GameScene () {
        SceneManager.LoadScene (1);
    }

    public void CreditScene () {
        SceneManager.LoadScene (3);
    }

    public void InstructionsScene () {
        SceneManager.LoadScene (4);
    }

    public void OptionsScene () {
        SceneManager.LoadScene (5);
    }

    public void MainMenu () {
        SceneManager.LoadScene (0);
    }

    public void Exit () {
        Application.Quit ();
    }

    public void Return () {
        BreedingCanvas.SetActive (false);
    }
}