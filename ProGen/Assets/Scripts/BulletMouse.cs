﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletMouse : MonoBehaviour {
    public GameObject BulletHole;
    public Transform MMCanvas;

    void Update () {

        if (Input.GetMouseButtonDown (0)) {
            GameObject newGO = Instantiate (BulletHole, MMCanvas);
            ((RectTransform) newGO.transform).position = Input.mousePosition;
            gameObject.GetComponent<AudioSource> ().Play ();
            Destroy (newGO, 2f);
        }
    }
}