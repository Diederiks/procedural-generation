﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NightDaySwitch : MonoBehaviour {
    public Button _NS;
    public Button _DS;
    public Light _mainL;
    [SerializeField] public float _nightIntensity = 0.16f;
    [SerializeField] public float _dayIntensity = 0.9f;
    // Start is called before the first frame update
    void Start () {
        _NS.onClick.AddListener (Night);
        _DS.onClick.AddListener (Day);
        _mainL.intensity = 1f;
    }

    public void Night () {
        _mainL.intensity = _nightIntensity;
    }
    public void Day () {
       _mainL.intensity = _dayIntensity;
    }
}