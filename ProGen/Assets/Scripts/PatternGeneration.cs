﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PatternGeneration : MonoBehaviour
{
     [SerializeField] RawImage genTex;
    [SerializeField] int PixelSpeed = 50;
    [SerializeField] int padding = 3;
    bool generatingL = false;
    public GameObject _LeCube;

    private void Start() {
        //Generate();
        StartCoroutine (GenerateOverTime ());
    }
    // public void Generate () {
    //     generatingL = !generatingL;
    //     if (generatingL) {

    //         Debug.Log ($"Generating");
    //         StartCoroutine (GenerateOverTime ());
    //     }
    // }

    IEnumerator GenerateOverTime () {
        Texture2D tex = new Texture2D (100, 100);
        Vector2Int point = new Vector2Int (100 / 2, 100 / 2);

        //Fill The Whole Thingy
        for (int w = 0; w <100; w++) {
            for (int h = 0; h < 100; h++) {
                tex.SetPixel (w, h, _LeCube.GetComponent<Renderer>().material.GetColor("_Color"));
            }
        }

        int counter = 999;
        while (counter > 0) {

            for (int i = 0; i < PixelSpeed; i++) {

                bool sideStep = Random.Range (0, 2) == 0 ? false : true;

                if (sideStep) {
                    //X Movement
                    point.x += Random.Range (0, 2) == 0 ? -1 : 1;
                } else {
                    //Y Movement
                    point.y += Random.Range (0, 2) == 0 ? -1 : 1;
                }

                tex.SetPixel (point.x, point.y, Color.white);
            }
            tex.Apply ();
            genTex.texture = tex;
            yield return null;

        }
        Texture2D finalizedTex = new Texture2D (tex.width, tex.height);

        //Fill The Whole Thingy
        for (int w = 0; w < 100; w++) {
            for (int h = 0; h < 100; h++) {
                finalizedTex.SetPixel (w, h, _LeCube.GetComponent<Renderer>().material.GetColor("_Color"));
            }
        }

        for (int h = 0; h < 100; h++) {
            for (int w = 0; w < 100; w++) {
                if (tex.GetPixel (w, h) == Color.white) {

                    //Center
                    finalizedTex.SetPixel (w, h, Color.black);
                    for (int i = 0; i < padding; i++) {
                        //Up
                        finalizedTex.SetPixel (w, h + i, Color.black);
                        //Down
                        finalizedTex.SetPixel (w, h - i, Color.black);
                        //Left
                        finalizedTex.SetPixel (w, h - i, Color.black);
                        //Right
                        finalizedTex.SetPixel (w, h + i, Color.black);
                    }
                }
            }
        }
        finalizedTex.Apply ();
        genTex.texture = finalizedTex;
    }
}
