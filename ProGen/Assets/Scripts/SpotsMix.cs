﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpotsMix : MonoBehaviour {
    public RawImage _MommyA;
    public RawImage _DaddyB;
    public RawImage _result;
    //Texture lerpedColor = Color.white;
    public Texture a;
    public Texture b;
    Renderer _randy;
    public Texture basemat;

    void Start () {
        _randy = GetComponent<Renderer> ();
    }
    void Update () {
        a = _MommyA.texture;
        b = _DaddyB.texture;
        _randy.material.SetTexture ("_MainTex", basemat);
        _result.texture = b;
    }

    public Texture2D Mix (Texture2D b, Texture2D a) {

        int startX = 0;
        int startY = b.height - a.height;

        for (int x = startX; x < b.width; x++) {

            for (int y = startY; y < b.height; y++) {
                Color bgColor = b.GetPixel (x, y);
                Color wmColor = a.GetPixel (x - startX, y - startY);

                Color final_color = Color.Lerp (bgColor, wmColor, wmColor.a / 0.5f);

                b.SetPixel (x, y, final_color);
            }
        }

        b.Apply ();
        return b;
    }
}