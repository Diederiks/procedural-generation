﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spots3 : MonoBehaviour {
    public Texture _Texan;
    public RawImage _GeneratenImage;
    Renderer _Rend2;
    public GameObject _Cuban;
 

    // Start is called before the first frame update
    void Start () {
        _Rend2 = GetComponent<Renderer> ();
    }

    public void Update () {
        _Texan = _GeneratenImage.texture;
        
        //_BasicHoe.SetTexture ("_Color", _Texan);
        _Cuban.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", _Texan);
    }
}