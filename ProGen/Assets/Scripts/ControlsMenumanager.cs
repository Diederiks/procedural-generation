﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlsMenumanager : MonoBehaviour
{
    public GameObject _objectP;
    public GameObject _viewP;
    public GameObject _weatherP;
    public GameObject _breedingP;
    public GameObject _dayDuskP;
    public GameObject _groundP;
    public GameObject _menusP;
    private void Start() {
        _objectP.SetActive(true);
        _viewP.SetActive(false);
        _weatherP.SetActive(false);
        _breedingP.SetActive(false);
        _dayDuskP.SetActive(false);
        _groundP.SetActive(false);
        _menusP.SetActive(false);
    }
    
    public void ObjectActive(){
        _objectP.SetActive(true);
        _viewP.SetActive(false);
        _weatherP.SetActive(false);
        _breedingP.SetActive(false);
        _dayDuskP.SetActive(false);
        _groundP.SetActive(false);
        _menusP.SetActive(false);
    }

    public void ViewActive(){
        _objectP.SetActive(false);
        _viewP.SetActive(true);
        _weatherP.SetActive(false);
        _breedingP.SetActive(false);
        _dayDuskP.SetActive(false);
        _groundP.SetActive(false);
        _menusP.SetActive(false);
    }

    public void WeatherActive(){
        _objectP.SetActive(false);
        _viewP.SetActive(false);
        _weatherP.SetActive(true);
        _breedingP.SetActive(false);
        _dayDuskP.SetActive(false);
        _groundP.SetActive(false);
        _menusP.SetActive(false);
    }

    public void BreedingActive(){
        _objectP.SetActive(false);
        _viewP.SetActive(false);
        _weatherP.SetActive(false);
        _breedingP.SetActive(true);
        _dayDuskP.SetActive(false);
        _groundP.SetActive(false);
        _menusP.SetActive(false);
    }

    public void DayDuskActicve(){
        _objectP.SetActive(false);
        _viewP.SetActive(false);
        _weatherP.SetActive(false);
        _breedingP.SetActive(false);
        _dayDuskP.SetActive(true);
        _groundP.SetActive(false);
        _menusP.SetActive(false);
    }

    public void GroundActive(){
        _objectP.SetActive(false);
        _viewP.SetActive(false);
        _weatherP.SetActive(false);
        _breedingP.SetActive(false);
        _dayDuskP.SetActive(false);
        _groundP.SetActive(true);
        _menusP.SetActive(false);
    }

    public void MenusActive(){
        _objectP.SetActive(false);
        _viewP.SetActive(false);
        _weatherP.SetActive(false);
        _breedingP.SetActive(false);
        _dayDuskP.SetActive(false);
        _groundP.SetActive(false);
        _menusP.SetActive(true);
    }
}
