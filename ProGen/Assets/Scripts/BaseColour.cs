﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseColour : MonoBehaviour {
    
    public static BaseColour instance;
    public Image _LeFish;
    void Start () {
        GenerateC ();
    }

    public void GenerateC () {
        _LeFish.GetComponent<Image>().color = Random.ColorHSV();
    }
}