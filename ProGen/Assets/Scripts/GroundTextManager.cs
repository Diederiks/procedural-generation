﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GroundTextManager : MonoBehaviour {
    public Material[] _groundMats;
    public GameObject _floor;
    // renderer _randy;
    public Button _spawnText;
    public int _rando;

    void Start () {
        _spawnText.onClick.AddListener (GenerateC);
    }

    public void GenerateC () {
        _rando = Random.Range (0, _groundMats.Length);
       // _floor.GetComponent<MeshRenderer> ().material.SetTexture ("_MainTex", _groundMats[_rando]);
        _floor.GetComponent<MeshRenderer>().material = _groundMats[_rando];
    }
}