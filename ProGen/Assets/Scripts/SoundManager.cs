﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    public AudioMixer audioMixer;
    public static SoundManager instance;

    private void Awake() {

        if(instance == null){
            instance = this;
        }
        else{
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }
    public void SetVolume(float volume){
        audioMixer.SetFloat("Volume", volume);
   }
   public void SetSoundFX(float volume){
        audioMixer.SetFloat("SoundFX", volume);
   }
   public void SetBGSound(float volume){
        audioMixer.SetFloat("Background", volume);
   }
}
