﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniMenu : MonoBehaviour
{
    public Button _pickFish;
    public GameObject _catalogue;
    // Start is called before the first frame update
    void Start()
    {
        _pickFish.onClick.AddListener(PicknPay);
        _catalogue.SetActive(false);
    }

    public void PicknPay(){
        _catalogue.SetActive(true);
    }

    public void PickMe(){
        _catalogue.SetActive(false);
    }
}
