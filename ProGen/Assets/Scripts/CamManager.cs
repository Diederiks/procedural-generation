﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CamManager : MonoBehaviour {
    public Camera _TopDown;
    public Camera _SideView;

    // Start is called before the first frame update
    void Start () {
        _TopDown.enabled = true;
        _SideView.enabled = false;
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown ("space")) {
            _TopDown.enabled = false;
            _SideView.enabled = true;
        }
        if (Input.GetKeyUp ("space")) {
            _TopDown.enabled = true;
            _SideView.enabled = false;
        }
    }
}