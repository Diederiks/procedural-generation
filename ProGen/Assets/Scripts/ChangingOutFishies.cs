﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangingOutFishies : MonoBehaviour
{
    public Image _originalImage;
    public Button _FishButton;
    public Image _EnclosureImage;

    public void ChangeItUp(){
        _EnclosureImage.GetComponent<Image>().color = _originalImage.GetComponent<Image>().color;
    }
}
