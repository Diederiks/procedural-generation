# Procedural generation
The purpose of this tool is to create procedurally generated materials for any object. 

I use this tool for a koi management game to generate the koi skin as well as the future generations that get breeded.

The tool allows the object to have a base color with spots generated from noise on top.
This tool also allows the user to combine two parent objects into a child, therefor the child would be a mixture of both parents.

I used the shadergraph in creating this tool along with the URP. The shadergraph mizes the materials and spots of the parents and is then called again later in script.

To use this tool you will need the URP and shadergraph installed.
simply apply the created materials to the parent objects and the child and the corrosponding scripts.

I used no tutorials in the making of this. none. 

Thats about it, have fun.
